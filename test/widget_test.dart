import 'package:flutter_test/flutter_test.dart';
import 'package:movie_app/main.dart';

void main() {
  testWidgets('MovieApp smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MovieApp());

    // Verify that our initial state is showing the "Popular Movies" text.
    expect(find.text('Popular Movies'), findsOneWidget);
  });
}
