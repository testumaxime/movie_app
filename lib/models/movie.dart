class Movie {
  final int id;
  final String name;
  final String permalink;
  final String? startDate;
  final String? endDate;
  final String? country;
  final String network;
  final String? status;
  final String imageThumbnailPath;

  Movie({
    required this.id,
    required this.name,
    required this.permalink,
    this.startDate,
    this.endDate,
    this.country,
    required this.network,
    this.status,
    required this.imageThumbnailPath,
  });

  factory Movie.fromJson(Map<String, dynamic> json) {
    return Movie(
      id: json['id'] ?? 0,
      name: json['name'] ?? 'Unknown',
      permalink: json['permalink'] ?? 'unknown',
      startDate: json['start_date'],
      endDate: json['end_date'],
      country: json['country'],
      network: json['network'],
      status: json['status'],
      imageThumbnailPath: json['image_thumbnail_path'] ?? 'https://via.placeholder.com/150',
    );
  }
}
